﻿using System;
using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Player hunger statistics
    /// </summary>
    [Serializable]
    public class Hunger : BaseVital
    {
        /// <summary>
        /// Current hunger value
        /// </summary>
        public float Value = 100f;
        /// <summary>
        /// Maximum hunger value
        /// </summary>
        public float MaxValue = 100f;
        /// <summary>
        /// Rate that hunger falls naturally
        /// </summary>
        public float HungerFallMultiplier = 2f;


        /// <summary>
        /// Increase the player's hunger
        /// </summary>
        public void UpdateHunger()
        {
            // Reset hunger to maximum if exceeded
            if (Value >= MaxValue)
            {
                Value = MaxValue;
            }

            // Increase the player's hungriness
            if (Value > 0)
            {
                Value -= Time.deltaTime * HungerFallMultiplier;
            }
            // Decrease health when fully hungry
            else
            {
                // Reset hunger if negative
                Value = 0f;

                parentPlayerVitals.Health.DecreaseHealth(Time.deltaTime * parentPlayerVitals.Health.HealthFallHungryMultiplier);
            }
        }
    }
}
