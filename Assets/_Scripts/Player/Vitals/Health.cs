﻿using System;

namespace RPGBase
{
    /// <summary>
    /// Player health statistics
    /// </summary>
    [Serializable]
    public class Health : BaseVital
    {
        /// <summary>
        /// Current health value
        /// </summary>
        public float Value = 100f;
        /// <summary>
        /// Maximum health value
        /// </summary>
        public float MaxValue = 100f;
        /// <summary>
        /// Rate that health falls from being completely thirsty
        /// </summary>
        public float HealthFallThirstyMultiplier = 1.0f;
        /// <summary>
        /// Rate that health falls from being completely hungry
        /// </summary>
        public float HealthFallHungryMultiplier = 1.0f;


        /// <summary>
        /// Decrease the player's health
        /// </summary>
        /// <param name="decreaseHealth">Amount of damage inflicted</param>
        public void DecreaseHealth(float decreaseHealth)
        {
            Value -= decreaseHealth;
        }


        /// <summary>
        /// Check whether the player is alive
        /// </summary>
        public void CheckHealth()
        {
            if (Value <= 0)
            {
                Die();
            }
        }


        /// <summary>
        /// Handle the player's death
        /// </summary>
        public void Die()
        {
            // Player can only die once
            if (!parentPlayerVitals.Alive) return;

            Value = 0f;

            // Indicate player death
            parentPlayerVitals.Alive = false;
        }
    }
}
