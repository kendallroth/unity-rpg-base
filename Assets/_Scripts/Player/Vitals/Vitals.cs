﻿using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Player vitals and statistics
    /// </summary>
    [RequireComponent(typeof(PlayerController))]
    public class Vitals : MonoBehaviour
    {
        /// <summary>
        /// Reference to player controller
        /// </summary>
        [HideInInspector]
        public PlayerController Controller;
        /// <summary>
        /// Player health vital
        /// </summary>
        public Health Health;
        /// <summary>
        /// Player hunger vital
        /// </summary>
        public Hunger Hunger;
        /// <summary>
        /// Player stamina vital
        /// </summary>
        public Stamina Stamina;
        /// <summary>
        /// Player thirst vital
        /// </summary>
        public Thirst Thirst;
        /// <summary>
        /// Player alive vital
        /// </summary>
        public bool Alive = true;
        /// <summary>
        /// Whether player is mortal (vitals don't decrease otherwise)
        /// </summary>
        public bool Mortal = true;


        void Start()
        {
            Controller = GetComponent<PlayerController>();
            Health.Init(this);
            Hunger.Init(this);
            Stamina.Init(this);
            Thirst.Init(this);
        }


        void Update()
        {
            // DEBUG
            if (!Mortal) return;

            Hunger.UpdateHunger();
            Thirst.UpdateThirst();
            Stamina.UpdateStamina();
            Health.CheckHealth();
        }
    }
}
