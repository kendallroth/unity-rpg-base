﻿using System;
using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Player stamina statistics
    /// </summary>
    [Serializable]
    public class Stamina : BaseVital
    {
        /// <summary>
        /// Current stamina value
        /// </summary>
        public float Value = 100f;
        /// <summary>
        /// Maximum stamina value
        /// </summary>
        public float MaxValue = 100f;
        /// <summary>
        /// Whether the player has any stamina
        /// </summary>
        public bool NoStamina = false;
        /// <summary>
        /// Rate that stamina falls while walking
        /// </summary>
        public float StaminaWalkingFallMultiplier = 0.2f;
        /// <summary>
        /// Rate that stamina falls while running
        /// </summary>
        public float StaminaRunningFallMultiplier = 2f;
        /// <summary>
        /// Rate that stamina regenerates while resting
        /// </summary>
        public float StaminaRegenerationMultiplier = 1f;

        // TODO: Add ability to decrease stamina SLOWLY while walking
        // TODO: Add ability to decrease stamina when jumping

        /// <summary>
        /// Decrease player stamina from movement
        /// </summary>
        public void UpdateStamina()
        {
            // Running expends more stamina than walking
            if (parentPlayerVitals.Controller.Running)
            {
                ExpendStamina(Time.deltaTime * StaminaRunningFallMultiplier);
            }
            else if (parentPlayerVitals.Controller.Walking)
            {
                ExpendStamina(Time.deltaTime * StaminaWalkingFallMultiplier);
            }
            // Stamina is regenerated only while standing still
            else if (!parentPlayerVitals.Controller.Moving)
            {
                RegenerateStamina();
            }
        }


        /// <summary>
        /// Expend stamina from a related player action
        /// <param name="staminaDrain">Amount of stamina drained by action</param>
        /// </summary>
        public void ExpendStamina(float staminaDrain)
        {
            // Reset stamina to maximum if exceeded (still should fall after)
            if (Value > MaxValue)
            {
                Value = MaxValue;
            }

            // Drain stamina
            if (Value > 0)
            {
                Value = Value - staminaDrain;
                NoStamina = false;
            }
            // Prevent player from running with no stamina
            else
            {
                // Indicate that the player has no stamina left
                NoStamina = true;
                // parentPlayerVitals.Controller.movementSettings.NoStamina = true;

                // Reset stamina if negative
                Value = 0;
            }
        }


        /// <summary>
        /// Regenerate stamina while not moving
        /// </summary>
        public void RegenerateStamina()
        {
            if (Value >= MaxValue) return;

            Value += Time.deltaTime * StaminaRegenerationMultiplier;
        }
    }
}
