﻿using System;
using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Player thirst statistic
    /// </summary>
    [Serializable]
    public class Thirst : BaseVital
    {
        /// <summary>
        /// Current thirst value
        /// </summary>
        public float Value = 100f;
        /// <summary>
        /// Maximum thirst value
        /// </summary>
        public float MaxValue = 100f;
        /// <summary>
        /// Rate that thirst drops natrually
        /// </summary>
        public float ThirstDropMultiplier = 2f;


        /// <summary>
        /// Increase the player's thirst
        /// </summary>
        public void UpdateThirst()
        {
            // Reset thirst to maximum if exceeded (still should fall after)
            if (Value >= MaxValue)
            {
                Value = MaxValue;
            }

            // Increase the player's thirst
            if (Value > 0)
            {
                Value -= Time.deltaTime * ThirstDropMultiplier;
            }
            // Decrease health when fully thirsty
            else
            {
                // Reset thirst if negative
                Value = 0f;

                parentPlayerVitals.Health.DecreaseHealth(Time.deltaTime * parentPlayerVitals.Health.HealthFallThirstyMultiplier);
            }
        }
    }

}
