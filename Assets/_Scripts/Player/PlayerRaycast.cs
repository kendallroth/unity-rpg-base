﻿using UnityEngine;
using UnityEngine.UI;

namespace RPGBase
{
    public class PlayerRaycast : MonoBehaviour
    {
        [SerializeField] private float rayLength = 2f;
        [SerializeField] private Camera camera;
        [SerializeField] private RawImage crosshair;
        [SerializeField] private LayerMask interactableLayersMask;
        [SerializeField] private GameObject interactionPanel;

        private GameObject raycastedObj;

        // Update is called once per frame
        void Update()
        {
            RaycastHit hit;
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            forward = camera.transform.forward;

            // Reset object hit by previous raycast
            raycastedObj = null;

            // Use center of camera as the collision ray origin (not player)
            Ray viewportRay = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            // DEBUG: Indicate where player is looking
            Debug.DrawRay(viewportRay.origin, viewportRay.direction * rayLength, Color.red);

            // Player should only check for interactions on specific layers (not terrain, water, etc)
            if (Physics.Raycast(viewportRay, out hit, rayLength, interactableLayersMask))
            {
                // TODO: Expand to only handle interactable items
                print("Interacted with " + hit.collider.name);
            }
        }
    }
}
