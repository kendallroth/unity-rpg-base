﻿using System;
using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Advanced player controller settings
    /// </summary>
    [Serializable]
    public class AdvancedSettings
    {
        /// <summary>
        /// Distance for checking whether the controller is grounded (0.01f works best)
        /// </summary>
        public float GroundCheckDistance = 0.01f;
        /// <summary>
        /// Distance from the player to the ground
        /// </summary>
        public float StickToGroundHelperDistance = 0.5f;
        /// <summary>
        /// Rate that the player will stop when there is no input
        /// </summary>
        public float SlowDownRate = 20f;
        /// <summary>
        /// Whether player can control their direction while in the air
        /// </summary>
        public bool AirControl = false;
        /// <summary>
        /// Player shell (collider) offset to avoid collisions (0.1f works best, reduce radius by same ratio)
        /// </summary>
        [Tooltip("Set it to 0.1 or more if you get stuck in wall")]
        public float ShellOffset;
    }
}
