﻿using System;
using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Player movement settings
    /// </summary>
    [Serializable]
    public class MovementSettings
    {
        /// <summary>
        /// Player walking speed
        /// </summary>
        public float ForwardSpeed = 3.5f;
        /// <summary>
        /// Player backpeddling speed
        /// </summary>
        public float BackwardSpeed = 2.0f;
        /// <summary>
        /// Player strafing left/right speed
        /// </summary>
        public float StrafeSpeed = 2.0f;
        /// <summary>
        /// Player running speed multiplier
        /// </summary>
        public float RunMultiplier = 2.0f;
        /// <summary>
        /// Force applied to player when jumping
        /// </summary>
        public float JumpForce = 40f;
        /// <summary>
        /// Current player speed
        /// </summary>
        [HideInInspector]
        public float CurrentTargetSpeed = 8f;

        /// <summary>
        /// Key for player running
        /// </summary>
        public KeyCode RunKey = KeyCode.LeftShift;
        /// <summary>
        /// Key for player jumping
        /// </summary>
        public KeyCode JumpKey = KeyCode.Space;

        /// <summary>
        /// Player slope affects movement speed
        /// </summary>
        public AnimationCurve SlopeCurveModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), new Keyframe(0.0f, 1.0f), new Keyframe(90.0f, 0.0f));

        private PlayerController controller;


        /// <summary>
        /// Initialize scene references
        /// </summary>
        /// <param name="controller">Player controller</param>
        public void Init(PlayerController controller)
        {
            this.controller = controller;
        }


        /// <summary>
        /// Determine player speed (not direction) from player input
        /// </summary>
        /// <param name="input">Initial speed vector</param>
        public void UpdateDesiredTargetSpeed(Vector2 input)
        {
            // No input means the player is neither walking nor running
            if (input == Vector2.zero)
            {
                // TODO: Determine if this works WHILE falling (ie. not because of jumping)
                controller.Running = false;
                controller.Walking = false;

                return;
            }

            // Strafe movement
            if (input.x > 0 || input.x < 0)
            {
                CurrentTargetSpeed = StrafeSpeed;
            }
            // Backwards movement
            if (input.y < 0)
            {
                CurrentTargetSpeed = BackwardSpeed;
            }
            // Forwards movement (handled after to ensure that if both strafing/forward the right speed is chosen)
            if (input.y > 0)
            {
                CurrentTargetSpeed = ForwardSpeed;
            }

            // Player can only run while they have stamina
            if (Input.GetKey(RunKey) && controller.CanRun)
            {
                CurrentTargetSpeed *= RunMultiplier;
                controller.Running = true;
                controller.Walking = false;
            }
            else
            {
                controller.Running = false;
                controller.Walking = true;
            }
        }
    }
}
